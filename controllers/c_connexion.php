<?php
if (!isset($_SESSION)){
session_start();
}
if (!isset($_REQUEST['action'])) {
    $_REQUEST['action'] = 'demandeConnexion';
}
$action = $_REQUEST['action'];
switch ($action) {
    case 'demandeConnexion':
    {
        include("views/v_connexion.php");
        break;
    }
    case 'valideConnexion':
    {
        $username = $_REQUEST['username'];
        $mdp = $_REQUEST['mdp'];
        $membre = $pdo->getInfosMembre($username, $mdp);
        if (!is_array($membre)) {
            echo "Login ou mot de passe incorrect";
            include("views/v_connexion.php");
        } else {
            $_SESSION['id'] = $membre['id'];
            $_SESSION['nom'] = $membre['nom'];
            $_SESSION['prenom'] = $membre['prenom'];
            header("Location: index.php?uc=gerer&action=accueil");
        }
        break;
    }
    case 'deconnexion':
        {
            session_destroy();
            header("Location: index.php");
            break;
        }
    default :
    {
        include("views/v_connexion.php");
        break;
    }
}
